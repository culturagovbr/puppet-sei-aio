# SEI AIO

Módulo Puppet SEI AIO (All-in-One).

#### Tabela de conteudo

1. [Sobre](#sobre)
2. [Autores](#autores)
3. [Colaboradores](#colaboradores)
4. [Coordenadores](#coordenadores)
5. [Projeto](#projeto)
6. [Requisitos](#requisitos)
7. [Classes](#classes)
8. [GIT](#git)
9. [Setup](#setup)
10. [Acesso](#acesso)
11. [Pendente](#pendente)
8. [Referências](#referencias)

## Sobre

Este módulo puppet foi desenvolvimento com o objetivo de facilitar a instalação do SEI em ambiente virtualizado para fins de teste, treinamento ou mesmo para produção em pequenos ambientes.

Para usar este módulo é necessário ter acesso ao código fonte do SEI préviamente.

## Autores

Desenvolvedores e criadores do módulo

  * Guto Carvalho (gutocarvalho@instruct.com.br)
  * Miguel Filho (gutocarvalho@instruct.com.br)

## Colaboradores

Profissionais que deram apoio na revisão e testes do módulo

  * Thiago Ferreira

## Coordenadores

Coordenadores do projeto SEI na CGTI-MINC
  
  * Diego Aguilera
  * Rogério Pereira
  * Christian Miranda
  * Francisco Morais
  
## Projeto

O desenvolvimento deste módulo ocorreu através de uma iniciativa do Ministério da Cultura e Instruct durante o projeto de implantação de SEI no MINC em 2015.

## Requisitos

Do sistema operacional

  * Esse módulo deve ser utilizado em CentOS 6

Da versão do puppet

  * Esse módulo deve ser executado com Puppet 4

Do módulo puppet

  * Este módulo depende do módulo puppetlabs-apache
  * Este módulo depende do módulo puppetlabs-mysql
  * Este módulo depende do módulo puppetlabs-stdlib
  * Este módulo depende do módulo puppetlabs-vcsrepo
  * Este módulo depende do módulo puppetlabs-staging
  * Este módulo depende do módulo puppetlabs-concat
  * Este módulo depende do módulo puppetlabs-inifile
  
Do hypervisor

  * Hypervisor Virtualbox ou VMWARE Workstation
    * Pelo menos 4 gigas de RAM para subir a VM
    * Pelo menos 10 gigas de disco para subir a VM
    * VM com CentOS 6 instalado
    * Acesso a internet para instalação de pacotes

Do código fonte

  * Código fonte do SEI disponível em algum repositório GIT 

## Classes

### O que esse modulo gerencia?

#### classe seaio::cron

  * Agendamento cron para script AgendamentoTarefaSEI.php
  * Agendamento cron para script AgendamentoTarefaSIP.php
  * Agendamento cron para limpeza do diretório ${docroot}/sei/upload/*
  * Agendamento cron para limpeza do diretório ${docroot}/sip/upload/*

#### classe seaio::database

  * Instalação do MariaDB Server (via puppetlabs-mysql)
  * Configuração do MYSQL Server
  * Criação de base de dados SEI
  * Cria estrutura da base de dados do SEI
  * Define nome da organização em tabelas do banco SEI
  * Criação de base de dados SIP
  * Cria estrutura da base de dados do SIP
  * Define nome da organização em tables do banco SIP
  * Define página inicial do SIP na base SIP
  * Define página incial do SEI na base SEI
  * Gerencia do serviço do MariaDB

#### classe seiaio::fonts

  * Instala pacotes
    *  openssl, libxml2, curl, cabextract
    *  fontconfig, xorg-x11-font-utils
  * Faz download do pacote msttcore-fonts-installer-2.6-1.noarch.rpm
  * Instala pacote msttcore-fonts-installer-2.6-1.noarch.rpm
  
#### classe seiaio::httpd

  * Instalação do Apache HTTPd (via puppetlabs-apache)
  * Configurações de do MOD PREFORK
  * Instalação do apache::mod::php
  * Instalação do apache::mod::rewrite
  * Instalação do apache::mod::ssl
  * Criacao de VHOST ${dominio}
    * Configuração de alias /sip
    * Configuração de alias /infra_js
    * Configuração de alias /infra_css
    * Configuração de alias /infra_php
    * Configurações de PHP para vhost
  * Criação do diretório ${docroot} base para o SEI
  * Gerenciar o serviço do httpd
  * Configuração de perms para diretório base
  * Criação do diretório ${seidados} 
  * Ajusta de permissão do binário 'wkhtmltopdf-amd64'

#### classe seiaio::jdk

  * Instalação do JDK para o tomcat e jetty
 
#### clase seiaio::jodc

  * Criação do group tomcat
  * Criação do usuário tomcat
  * Criação do diretório /opt/jodconverter
  * Ajuste nas permissões do diretório /opt/jodconverter
  * Instalação do tomcat
  * Instalação do Jod Converter
  * Instalação do LibreOffice
  * Configuração do serviço do soffice
  * Gerência do servico soffice
  * Gerência do serviço tomcat/jodc

#### classe seiaio::memcached

  * Instalação do pacote memcached
  * Gerência do serviço memcached

#### classe seiaio::php

  * Instalação de pacotes php
    * php-cli
    * php-pear
    * php-bcmath
    * php-gd
    * php-imap (confirmar dependencia)
    * php-ldap
    * php-mbstring
    * php-mysql
    * php-pdo
    * php-soap
    * php-xml
    * php-intl
    * php-odbc
    * php-snmp (confirmar dependencia)
    * php-xmlrpc
    * php-pspell
    * php-pecl-apc
    * php-pecl-apc-devel
    * php-pecl-memcache
    * php-zts
  * Instalação do pacote gcc (para compilacao do uploadprogress)
  * Instalação/compilação do uploadprogress via pecl
  * Configuração do uploadprogess.ini para carregar no php.d
  * Ajustes no php.ini para scripts do cron
    * post_max_size
    * default_charset
    * short_open_tag
    * default_socket_timeout
    * include_path
    * session_gc_maxlifetime
    * date_timezone 
 
#### classe seiaio::seifontes 

  * Download dos fontes do SEI de repositório GIT

#### classe seiaio::solr 

  * Criação do grupo solr
  * Criação do usuário solr
  * Criação do diretório /opt/solr
  * Ajuste nas permissões do diretório /opt/solr
  * Define configurações de inicilização do jetty
  * Define configurações de inicialização do solr
  * Cria diretório /var/log/solr
  * Cria diretório /opt/solr/indices
  * Cria diretorio e configura indices "protocolos"
  * Cria diretório e configura indices "base de conhecimento"
  * Cria diretório e configura indices "publicacao"
  * Executa script para criação de indices no solrd

#### classe seiaio::seiconf

  * Criação dinâmica do arquivo ConfiguracaoSEI.php
  * Criação dinâmica do arquivo ConfiguracaoSip.php

## Uso das classes

### classe principal

Neste módulo temos uma classe pública, basta chamá-la que o sei será instalado.

#### declarando classe seiaio (classe principal)

```puppet
  class { seiaio:
    docroot            => '/var/www/html/appsei',
    diretorio_seidados => '/var/www/seidados',
    root_mysql         => 'OGbBv1rf87zNAr198cOA3ygyj',
    sei_mysql_pass     => 'mA8357mHmsJszBl',
    sip_mysql_pass     => 'mA8357mHmsJszBl',
    mysql_ipaddr       => 'localhost',
    dominio            => 'sei.organizacao.gov.br',
    git_repo           => 'git@bitbucket.org:instruct/seiaio.git',
    sigla_organizacao  => 'ORG',
    nome_organizacao   => 'Organizacao Federal',
}
```
### classes privadas

As classes privadas normalmente não são chamadas diretamente, a classe principal já faz a chamada a estas classes.

#### declarando classe seiaio::fontes (classe privada)

```puppet
class { 'seiaio::seifontes':
  docroot      => '/var/www/html/appsei',
  sei_git_repo => 'git@bitbucket.org:instruct/seiaio.git',
}
```

#### declarando classe seiaio::httpd (classe privada)

```puppet
 class { 'seiaio::httpd':
   docroot  => '/var/www/html/appsei',
   owner    => 'apache',
   group    => 'apache',
   dominio  => 'sei.dominio.gov.br',
   seidados => '/var/www/seidados',
 }
```

#### declarando classe seiaio::database (classe privada)

```puppet
class { 'seiaio::database':
  root_mysql        => 'senha',
  sei_mysql_pass    => 'senha',
  sip_mysql_pass    => 'senha',
  dominio           => 'sei.dominio.gov.br',
  mysql_ipaddr      => 'ip',
  sigla_organizacao => 'ORG',
  nome_organizacao  => 'ORGANIZACAO',
}
```

#### delcarando classe sei::memcached (classe privada)

    include seiaio::memcached
  
#### delcarando classe sei::jdk (classe privada)
   
    include seiaio::jdk

#### delcarando classe sei::fonts (classe privada)

    include seiaio::fonts

#### delcarando classe sei::php (classe privada)

```puppet
class { 'seiaio::php':
  docroot => '/var/www/html/appsei',
}
```  

#### delcarando classe sei::cron (classe privada)

```puppet
class { 'seiaio::cron':
  docroot => '/var/www/html/appsei',
}
```

#### delcarando classe sei::solr (classe privada) 

```puppet
class { 'seiaio::solr':
  dominio => 'sei.organizacao.gov.br',
}
```
  
#### delcarando classe sei::jodc (classe privada)
  
    include seiaio::jodc

#### delcarando classe sei::seiconf (classe privada)

```puppet 
class { 'seiaio::seiconf':
  docroot           => '/var/www/html/appsei',
  owner             => 'apache',
  group             => 'apache',
  dominio           => 'sei.organizacao.gov.br',
  seidados          => '/var/www/seidados',
  mysql_ipaddr      => '127.0.0.1',
  sei_mysql_pass    => 'senha',
  sip_mysql_pass    => 'senha',
  sigla_organizacao => 'GOV',
  nome_organizacao  => 'Organizacao',
}
```

## Git

O código do sei tem que estar em um repositório GIT com a seguinte estrutura na raiz do repositório:

    db
    infra_css
    infra_js
    infra_php
    sei
    sip
    
Se a estrutura não for essa, o módulo não irá funcionar.
    
## Setup

Em seu CentOS 6 instale o repositório do puppet

    # yum install http://yum.puppetlabs.com/el/6/PC1/x86_64/puppetlabs-release-pc1-0.9.2-1.el6.noarch.rpm -y

Depois instale o puppet

    # yum install puppet-agent -y

Você precisa instalar o módulo no diretório de módulos.

    # cd /etc/puppetlabs/code/environment/production/modules
    # git clone https://bitbucket.org/culturagovbr/puppet-sei-aio.git seiaio
    
Instale os modulos que são dependencia direta

    # puppet module install puppetlabs-apache
    # puppet module install puppetlabs-vcsrepo
    # puppet module install puppetlabs-inifile
    # puppet module install puppetlabs-mysql
    
Depois de instalar os módulos crie um manifest para chamar o módulo

    # vim /root/seiaio.pp

E adicione o conteúdo abaixo, ajustando as informações conforme seu ambiente

```puppet
class { seiaio:
  docroot            => '/var/www/html/appsei',
  diretorio_seidados => '/var/www/seidados',
  root_mysql         => 'OGbBv1rf87zNAr198cOA3ygyj',
  sei_mysql_pass     => 'mA8357mHmsJszBl',
  sip_mysql_pass     => 'mA8357mHmsJszBl',
  mysql_ipaddr       => 'localhost',
  dominio            => 'sei.organizacao.gov.br',
  git_repo           => 'git@bitbucket.org:instruct/seiaio.git',
  sigla_organizacao  => 'ORG',
  nome_organizacao   => 'Organizacao Federal',
}
```

Após criar o manifest execute o puppet contra o manifest

    # puppet apply /root/seiaio.pp
    
Aguarde a instalação do SEI, após a instalação use o manual do SEI para acessá-lo e configurá-lo.

Devido ao download do openjdk e do LibreOffice a instalação pode levar bastante tempo dependendo da sua conexão de internet. 

Em testes feitos em um link de 120 Mbits o puppet levou 21 minutos para finalizar a instalação do SEI, 18 minutos praticamente fazendo download.

Em testes com repositório local de pacotes RPM (sem necessidade de download da internet) a instalação toda pode variar entre 120 e 360 segundos. 

## Acesso

Após instalar o SEI será necessário configurar no /etc/host do seu desktop linux/unix/mac em que está rodando o hypervisor.

     ip_da_vm_rodando_sei sei.organizacao.gov.br

Isso é necessário para resolver o nome no navegador, depois de configurar o arquivo hosts, feche e abra o navegado e entre no sei. O usuário para acesso inicial é teste/teste. Siga o manual do SEI depois disto.

## Pendente

  1. Criar smoking testes para classes puppet
  2. Criar testes unitários para classes puppet
  3. Criar arquivo com metadados para o módulo

## Referências

Sobre o SEI/PEN

* https://processoeletronico.gov.br/projects/sei/wiki/PEN
* https://softwarepublico.gov.br/social/sei

Sobre o módulo

* https://bitbucket.org/culturagovbr/puppet-sei-aio
* https://bitbucket.org/instruct/puppet-sei-aio
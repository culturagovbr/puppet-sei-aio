class seiaio::seifontes (
  String $docroot = $::seiaio::params::httpd_sei_docroot,
  String $sei_git_repo,
  ) inherits seiaio::params {

    vcsrepo {  $docroot:,
    ensure   => latest,
    provider => git,
    revision => 'master',
    source   => $sei_git_repo
  }
}

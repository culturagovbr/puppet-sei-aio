class seiaio (
  String $owner              = $::seiaio::params::httpd_owner,
  String $group              = $::seiaio::params::httpd_group,
  String $docroot            = $::seiaio::params::httpd_sei_docroot,
  String $root_mysql         = $::seiaio::params::root_mysql,
  String $sei_mysql_pass     = $::seiaio::params::sei_mysql_pass,
  String $sip_mysql_pass     = $::seiaio::params::sip_mysql_pass,
  String $mysql_ipaddr       = $::seiaio::params::mysql_ipaddr,
  String $dominio            = $::seiaio::params::dominio,
  String $git_repo           = $::seiaio::params::sei_git_repo,
  String $sigla_organizacao  = $::seiaio::params::sigla_organizacao,
  String $nome_organizacao   = $::seiaio::params::nome_organizacao,
  String $diretorio_seidados = $::seiaio::params::httpd_sei_dados
    ) inherits seiaio::params {

  class { 'seiaio::seifontes':
      docroot      => $docroot,
      sei_git_repo => $git_repo,
  }

  class { 'seiaio::httpd':
    docroot  => $docroot,
    owner    => $owner,
    group    => $group,
    dominio  => $dominio,
    seidados => $diretorio_seidados,
  }

  class { 'seiaio::database':
    root_mysql        => $root_mysql,
    sei_mysql_pass    => $sei_mysql_pass,
    sip_mysql_pass    => $sip_mysql_pass,
    dominio           => $dominio,
    mysql_ipaddr      => $mysql_ipaddr,
    sigla_organizacao => $sigla_organizacao,
    nome_organizacao  => $nome_organizacao,
  }

  include seiaio::memcached
  include seiaio::jdk
  include seiaio::fonts

  class { 'seiaio::php':
    docroot => $docroot,
  }

  class { 'seiaio::cron':
    docroot => $docroot,
  }

  class { 'seiaio::solr':
    dominio => $dominio,
  }

  include seiaio::jodc

  class { 'seiaio::seiconf':
    docroot           => $docroot,
    owner             => $owner,
    group             => $group,
    dominio           => $dominio,
    seidados          => $diretorio_seidados,
    mysql_ipaddr      => $mysql_ipaddr,
    sei_mysql_pass    => $sei_mysql_pass,
    sip_mysql_pass    => $sip_mysql_pass,
    sigla_organizacao => $sigla_organizacao,
    nome_organizacao  => $nome_organizacao,
  }
}
